* Pytilt

Tool for reading your Tilt brewing hydrometer[1] in python on a
rasberry pi and send the data to a graphite server.

Inspired from [[https://github.com/atlefren/pytilt.git][https://github.com/atlefren/pytilt.git]] 

** Installation

#+BEGIN_SRC sh
git clone 
# Install pyhton-bluez
sudo apt-get install python-bluez
# Make the bluetooth interface accessible without being root: 
sudo setcap cap_net_raw+eip /usr/bin/python2.7
#+END_SRC

** Running

From the directory containing pytilt.py:
#+BEGIN_SRC sh
export PYTILT_GRAPHITE_SERVER=shuttle
python pytilt.py
#+END_SRC


** Troubleshooting

Check if RPi sees the Tilt
#+BEGIN_SRC sh
hcitool lescan
#+END_SRC

If you get this error:
~Set scan parameters failed: Input/output error~
Try resetting the bluetooth device:
#+BEGIN_SRC 
hciconfig hci0 down
hciconfig hci0 up
#+END_SRC


** Running Pytilt in the background and on System Start
-----------
0. edit pytilt.service, add your key and fix paths
1. copy pytilt.service to /lib/systemd/system/
2. sudo chmod 644 /lib/systemd/system/pytilt.service
3. sudo systemctl daemon-reload
4. sudo systemctl enable pytilt.service
5. sudo reboot


Acknowledgements
----------------
The code in blescan-py is adapted from https://github.com/switchdoclabs/iBeacon-Scanner-
The Tilt UUID-to-color mapping is taken from: https://github.com/tbryant/brewometer-nodejs
Systemd-config here: http://www.raspberrypi-spy.co.uk/2015/10/how-to-autorun-a-python-script-on-boot-using-systemd/


[1]: https://tilthydrometer.com/
